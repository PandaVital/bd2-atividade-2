-- Excluindo tabelas para recrialas
drop table if exists acessorio cascade;
drop table if exists veiculoAcessorio cascade;
drop table if exists veiculo cascade;
drop table if exists modelo cascade;
drop table if exists fabricante cascade;
drop table if exists telefones cascade;
drop table if exists endereco cascade;
drop table if exists cidade cascade;
drop table if exists estado cascade;
drop table if exists grupo cascade;
drop table if exists diaria cascade;


-- acessorio ( sigla_Pk, nome)
create table acessorio (
sigla_Pk	char(3) 	not null,
nome		varchar(40) 	not null,
constraint pk_acessorio
	primary key (sigla_Pk)
);
-- veiculoAcessorio(placa_Fk, sigla_Fk)
create table veiculoAcessorio (
placa_Pk_Fk 	varchar(7) 	not null,
sigla_Fk 	char(3) 	not null,
constraint pk_veiculoAcessorio
	primary key (placa_Pk_Fk)
);
-- veiculo(placa_Pk, quilometragem_Atual, ano_Fabricacao, data_venda(O), data_Aquisicao)
create table veiculo (
placa_Pk 		varchar(10) 	not null,
quilometragem_atual	integer	not null,
ano_Fabricacao		varchar(5)		not null,
data_venda		date,
data_aquisicao		date		not null,
constraint pk_veiculo
	primary key(placa_Pk)

);
-- modelo (codigo_Pk, nome)
create table modelo (
codigo_pk	integer		not null,
nome		varchar(44)	not null,
constraint pk_modelo
	primary key(codigo_pk)
);
-- fabricante (CNPJ_Pk, nome, telefone_Fk,endereco_Fk)
create table fabricante (
CNPJ		integer		not null,
nome		varchar(44)	not null,
telefone	integer		not null,
endereco	integer		not null,
constraint pk_fabricante
		primary key(CNPJ)
);
-- telefones(CNPJ_Pk_Fk, telefone)
create table telefones (
CNPJ		integer		not null,
telefone	integer		not null,
constraint pk_telefones
		primary key(CNPJ)
);
-- endereco(codigo_Pk, numero, logradouro, bairro, complemento(O), cidade_Fk)
create table endereco (
codigo_pk	integer		not null,
numero		smallint	not null,
logradouro	varchar(44)	not null,
bairro		varchar(20)	not null,
complemento	varchar(44),
cidade		char(2) 	not null,
constraint pk_endereco
	primary key(codigo_pk)
);
-- cidade (sigla_Pk, estado)
create table cidade (
sigla_pk	char(2)		not null,
nome	char(256)		not null,
constraint pk_cidade
	primary key(sigla_pk)
);
-- estado( sigla_Pk, nome)
create table estado (
sigla_pk	char(2)		not null,
nome		varchar(44)	not null,
constraint pk_estado
	primary key(sigla_pk)
);
-- grupo(codigo_PK, nome)
create table grupo (
codigo_PK     integer     not null,
nome     varchar(44)     not null,
constraint pk_grupo
    primary key (codigo_PK)
);
-- diaria(data_inicio_vigencia_PK, data_fim_vigencia, valor)
create table diaria (
data_inicio_vigencia_PK     date     not null,
data_fim_vigencia     date     not null,
valor     integer     not null,
constraint pk_diaria
    primary key (data_inicio_vigencia_PK)
);

--Atualização de tabelas

UPDATE diaria 
SET data_fim_vigencia = CURRENT_DATE + INTERVAL '1 day' 
where data_inicio_vigencia_pk = CURRENT_DATE;

--insert

INSERT INTO cidade(sigla_pk, nome)
VALUES ('SA', 'Salvador');

INSERT INTO cidade(sigla_pk, nome)
VALUES ('RJ', 'Rio de Janeiro');

INSERT INTO cidade(sigla_pk, nome)
VALUES ('SP', 'Sao Paulo');
 
INSERT INTO estado(sigla_pk, nome)
VALUES ('BA', 'Bahia');
INSERT INTO estado(sigla_pk, nome)
VALUES ('RS', 'Rio Grande do Sul');
INSERT INTO estado(sigla_pk, nome)
VALUES ('RJ', 'Rio de Janeiro');

INSERT INTO modelo(codigo_pk,nome)
VALUES (1,'Gol quadrado');
INSERT INTO modelo(codigo_pk,nome)
VALUES (2,'Ford ca');
INSERT INTO modelo(codigo_pk,nome)
VALUES (3,'Ferrari');

INSERT INTO endereco(codigo_pk,numero, logradouro, bairro, complemento, cidade)
VALUES (1,35, 'rua das freiras','brotas','em ferente ao mc donalds','SP');

INSERT INTO endereco(codigo_pk,numero, logradouro, bairro, complemento, cidade)
VALUES (2,25, 'rua dos santos','brotas','em ferente ao spa','BA');

INSERT INTO fabricante(cnpj,nome,telefone,endereco)
VALUES ('1545341358', 'ford',12121212, 1);

INSERT INTO veiculo(placa_pk,quilometragem_atual,ano_fabricação,data_aquisicao)
VALUES ('JDS-1231', 100,'2017','2019-02-05');

INSERT INTO veiculo(placa_pk,quilometragem_atual,ano_fabricação,data_aquisicao)
VALUES ('JHD-1531', 1000,'2020','2020-02-05');

INSERT INTO veiculo(placa_pk,quilometragem_atual,ano_fabricação,data_aquisicao)
VALUES ('FSS-9900', 10000,'2019','2021-02-05');

--Consulta

select * from modelo where nome like 'Gol%';

SELECT MAX(quilometragem_atual) as maior_kilometragem
FROM veiculo;
select MIN(quilometragem_atual) as menor_kilometragem
FROM veiculo;

-- Remoção de telefone da tabela fabricante
INSERT INTO fabricante(CNPJ, nome, telefone,endereco)
VALUES (123456789, 'Palio' ,12346789, 1);

INSERT INTO fabricante(CNPJ, nome, telefone,endereco)
VALUES (123456785, 'Chevrolet' ,12346781, 1);

INSERT INTO fabricante(CNPJ, nome, telefone,endereco)
VALUES (123456749, 'Ford' ,12346782, 1);

INSERT INTO fabricante(CNPJ, nome, telefone,endereco)
VALUES (123456783, 'Pegeout' ,12346783, 1);

INSERT INTO fabricante(CNPJ, nome, telefone,endereco)
VALUES (123456782, 'Ferari' ,12346784, 1);

INSERT INTO fabricante(CNPJ, nome, telefone,endereco)
VALUES (123456781, 'Palio' ,12346785, 1);

delete
from	fabricante
where	telefone = '12346789'

-- update nome fabricante

update 	fabricante
	set	nome = 'Toyota'
where	nome = 'Palio';
